def index():
    redirect(URL('prekes'))


# ---- Action for login/register/etc (required for auth) -----
def user():
    """
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


def populate_fresh_db():
    from gluon.contrib.populate import populate
    db.products.insert(name='pienas',price=2)
    db.products.insert(name='duona',price=0.9)
    db.products.insert(name='desra',price=5)
    db.products.insert(name='iphone', price=1000)

    populate(db.purchases, 10)

    return dict(
        purchases=db(db.purchases).select(),
        users=db(db.auth_user).select(),
        products=db(db.products).select(),
    )


def beautify_products(rows):
    # user_ids = [x.user_id for x in rows]
    # user_names = db(db.auth_user.id.belongs(user_ids)).select(db.auth_user.id, db.auth_user.first_name)
    # names = {x.id: x.first_name for x in user_names}
    product_ids = [x.products for x in rows]
    product_names = db(db.products.id.belongs(product_ids)).select()
    product_name = {x.id: x.title for x in product_names}
    result = UL(
        [LI(
            DIV(H5(product_name[x.product_id]),
                # B(names[x.users_]), BR(),
                B(x.name[0:40]),  # ribojam rodomo title ilgį iki 40
                BR(), x.price,
                ),
            # A("Rodyti visą info", _class='btn btn-info', _href=URL('post', args=x.id)),
            # A("Trinti posta", _class='btn btn-warning', _href=URL('post_del', args=x.id)),
            # A("Redaguoti", _class='btn btn-info', _href=URL('post', args=[x.id, 'edit']))
        )
            for x in rows]
    )
    return result


def prekes():
    # http://tiny.lt/pycrud
    rows = UL(
        db().select(db.products.ALL),  # db.posts.ALL atitinka SELECT *

    # UŽDUOTIS: padaryti gražų sąrašą su link'ais į post'o peržiūrą
    # rows = beautify_products(rows)

    A("Krepselin", _class='button btn btn-info', _href=URL('krepsys'))
    )


    return dict(rows=rows  # {'rows':rows}
                # naujo įrašo kūrimo link'as
                )
def krepsys():

    return
